﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AgricultureManagementSystem.Models.ViewModels
{
    public class DistributionVm
    {
        public int Id { get; set; }
        [Required]
        public System.DateTime Date { get; set; }
        [Required]
        public int FarmerId { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Area { get; set; }
        [Required]
        public int TypeId { get; set; }
        [Required]
        public int Quantity { get; set; }
    }
}