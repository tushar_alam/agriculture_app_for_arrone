﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AgricultureManagementSystem.Models.ViewModels
{
    public class ReceiveOfProductVm
    {
        public int Id { get; set; }

        public System.DateTime ArriveDate { get; set; }

        [Required]
        public string ProductName { get; set; }
    }
}