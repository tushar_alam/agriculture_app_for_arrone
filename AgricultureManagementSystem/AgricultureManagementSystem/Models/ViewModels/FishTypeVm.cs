﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AgricultureManagementSystem.Models.ViewModels
{
    public class FishTypeVm
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}