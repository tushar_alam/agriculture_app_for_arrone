﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AgricultureManagementSystem.Models.ViewModels
{
    public class FarmerVm
    {
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string MiddleName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Province { get; set; }
        [Required]
        public string Municipality { get; set; }
        [Required]
        public string SmallTown { get; set; }
        [Required]
        public System.DateTime BirthDate { get; set; }
        [Required]
        public string Gender { get; set; }
        [Required]
        public string IDNo { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string ContactNo { get; set; }
        public int Status { get; set; }
    }
}