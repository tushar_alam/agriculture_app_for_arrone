﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AgricultureManagementSystem.Models.ViewModels
{
    public class UserAccountVm
    {
        public int Id { get; set; }
        [Required]
        public int FarmerId { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        public int Status { get; set; }
    }
}