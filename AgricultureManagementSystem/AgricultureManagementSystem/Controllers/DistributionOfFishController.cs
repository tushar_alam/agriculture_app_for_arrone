﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AgricultureManagementSystem.Helper;
using AgricultureManagementSystem.Models;
using AgricultureManagementSystem.Models.ViewModels;

namespace AgricultureManagementSystem.Controllers
{
    [SessionExpire]
    public class DistributionOfFishController : Controller
    {
        private AMSEntities db = new AMSEntities();

        // GET: DistributionOfFish
        public ActionResult Index()
        {
            var distributionOfFish = db.DistributionOfFish.Include(d => d.Farmer).Include(d => d.FishType);
            return View(distributionOfFish.ToList());
        }

        // GET: DistributionOfFish/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DistributionOfFish distributionOfFish = db.DistributionOfFish.Find(id);
            if (distributionOfFish == null)
            {
                return HttpNotFound();
            }
            return View(distributionOfFish);
        }

        // GET: DistributionOfFish/Create
        public ActionResult Create()
        {
            ViewBag.FarmerId = new SelectList(db.Farmers, "Id", "FirstName");
            ViewBag.TypeId = new SelectList(db.FishTypes, "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DistributionVm distributionVm)
        {
            if (distributionVm != null)
            {
                var distribution = new DistributionOfFish
                {
                    Id = distributionVm.Id,
                    Date = distributionVm.Date,
                    FarmerId = distributionVm.FarmerId,
                    Address = distributionVm.Address,
                    Area = distributionVm.Area,
                    FishTypeId = distributionVm.TypeId,
                    Quantity = distributionVm.Quantity,
                };

                db.DistributionOfFish.Add(distribution);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FarmerId = new SelectList(db.Farmers, "Id", "FirstName", distributionVm.FarmerId);
            ViewBag.TypeId = new SelectList(db.FishTypes, "Id", "Name", distributionVm.TypeId);
            return View(distributionVm);
        }

        // GET: DistributionOfFish/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DistributionOfFish distributionVm = db.DistributionOfFish.Find(id);
            if (distributionVm == null)
            {
                return HttpNotFound();
            }

            var distribution = new DistributionVm
            {
                Id = distributionVm.Id,
                Date = distributionVm.Date,
                FarmerId = distributionVm.FarmerId,
                Address = distributionVm.Address,
                Area = distributionVm.Area,
                TypeId = distributionVm.FishTypeId,
                Quantity = distributionVm.Quantity,
            };
            ViewBag.FarmerId = new SelectList(db.Farmers, "Id", "FirstName", distribution.FarmerId);
            ViewBag.TypeId = new SelectList(db.FishTypes, "Id", "Name", distribution.TypeId);
            return View(distribution);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DistributionVm distributionVm)
        {
            if (distributionVm != null)
            {
                var distribution = new DistributionOfFish
                {
                    Id = distributionVm.Id,
                    Date = distributionVm.Date,
                    FarmerId = distributionVm.FarmerId,
                    Address = distributionVm.Address,
                    Area = distributionVm.Area,
                    FishTypeId = distributionVm.TypeId,
                    Quantity = distributionVm.Quantity,
                };
                db.Entry(distribution).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FarmerId = new SelectList(db.Farmers, "Id", "FirstName", distributionVm.FarmerId);
            ViewBag.TypeId = new SelectList(db.FishTypes, "Id", "Name", distributionVm.TypeId);
            return View(distributionVm);
        }

        // GET: DistributionOfFish/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DistributionOfFish distributionOfFish = db.DistributionOfFish.Find(id);
            if (distributionOfFish == null)
            {
                return HttpNotFound();
            }
            return View(distributionOfFish);
        }

        // POST: DistributionOfFish/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DistributionOfFish distributionOfFish = db.DistributionOfFish.Find(id);
            db.DistributionOfFish.Remove(distributionOfFish);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
