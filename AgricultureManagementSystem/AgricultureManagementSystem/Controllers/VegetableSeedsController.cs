﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AgricultureManagementSystem.Helper;
using AgricultureManagementSystem.Models;
using AgricultureManagementSystem.Models.ViewModels;

namespace AgricultureManagementSystem.Controllers
{
    [SessionExpire]
    public class VegetableSeedsController : Controller
    {
        private AMSEntities db = new AMSEntities();

        // GET: VegetableSeeds
        public ActionResult Index()
        {
            return View(db.VegetableSeeds.ToList());
        }

        // GET: VegetableSeeds/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VegetableSeed vegetableSeed = db.VegetableSeeds.Find(id);
            if (vegetableSeed == null)
            {
                return HttpNotFound();
            }
            return View(vegetableSeed);
        }

        // GET: VegetableSeeds/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SeedVm vegetableSeedVm)
        {
            var vegetableSeed = new VegetableSeed
            {
                Id = vegetableSeedVm.Id,
                Name = vegetableSeedVm.Name,
            };
            if (ModelState.IsValid)
            {
                db.VegetableSeeds.Add(vegetableSeed);
                db.SaveChanges();
                TempData["SuccessMessage"] = "Created successfully";
                return RedirectToAction("Index");
            }
            TempData["ErrorMessage"] = "Create faild";
            return View(vegetableSeed);
        }

        // GET: VegetableSeeds/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var vegetableSeedVm = db.VegetableSeeds.Find(id);

            var vegetableSeed = new SeedVm
            {
                Id = vegetableSeedVm.Id,
                Name = vegetableSeedVm.Name,
            };
            if (vegetableSeed == null)
            {
                return HttpNotFound();
            }
            return View(vegetableSeed);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SeedVm vegetableSeedVm)
        {
            var vegetableSeed = new VegetableSeed
            {
                Id = vegetableSeedVm.Id,
                Name = vegetableSeedVm.Name,
            };
            if (ModelState.IsValid)
            {
                db.Entry(vegetableSeed).State = EntityState.Modified;
                db.SaveChanges();
                TempData["SuccessMessage"] = "update successfully";
                return RedirectToAction("Index");
            }
            TempData["ErrorMessage"] = "update faild";
            return View(vegetableSeed);
        }

        // GET: VegetableSeeds/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VegetableSeed vegetableSeed = db.VegetableSeeds.Find(id);
            if (vegetableSeed == null)
            {
                return HttpNotFound();
            }
            return View(vegetableSeed);
        }

        // POST: VegetableSeeds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var obj = db.VegetableSeeds.Find(id);
            try
            {
                db.VegetableSeeds.Remove(obj);
                db.SaveChanges();
                TempData["SuccessMessage"] = " deleted successfully";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                if (obj == null)
                {
                    return HttpNotFound();
                }
                TempData["ErrorMessage"] = "You can't delete this.";
                return View(obj);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
