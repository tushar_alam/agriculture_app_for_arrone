﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AgricultureManagementSystem.Helper;
using AgricultureManagementSystem.Models;
using AgricultureManagementSystem.Models.ViewModels;

namespace AgricultureManagementSystem.Controllers
{
    [SessionExpire]
    public class RiceSeedsController : Controller
    {
        private AMSEntities db = new AMSEntities();

        // GET: RiceSeeds
        public ActionResult Index()
        {
            return View(db.RiceSeeds.ToList());
        }

        // GET: RiceSeeds/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RiceSeed riceSeed = db.RiceSeeds.Find(id);
            if (riceSeed == null)
            {
                return HttpNotFound();
            }
            return View(riceSeed);
        }

        // GET: RiceSeeds/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SeedVm riceSeedVm)
        {
            var riceSeed = new RiceSeed
            {
                Id = riceSeedVm.Id,
                Name = riceSeedVm.Name,
            };

            if (ModelState.IsValid)
            {
                db.RiceSeeds.Add(riceSeed);
                db.SaveChanges();
                TempData["SuccessMessage"] = "Created successfully";
                return RedirectToAction("Index");
            }
            TempData["ErrorMessage"] = "Create faild";
            return View(riceSeed);
        }

        // GET: RiceSeeds/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var riceSeedVm = db.RiceSeeds.Find(id);

            var riceSeed = new SeedVm
            {
                Id = riceSeedVm.Id,
                Name = riceSeedVm.Name,
            };

            if (riceSeed == null)
            {
                return HttpNotFound();
            }
            return View(riceSeed);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SeedVm riceSeedVm)
        {
            var riceSeed = new RiceSeed
            {
                Id = riceSeedVm.Id,
                Name = riceSeedVm.Name,
            };
            if (ModelState.IsValid)
            {
                db.Entry(riceSeed).State = EntityState.Modified;
                db.SaveChanges();
                TempData["SuccessMessage"] = "update successfully";
                return RedirectToAction("Index");
            }
            TempData["ErrorMessage"] = "update faild";
            return View(riceSeed);
        }

        // GET: RiceSeeds/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RiceSeed riceSeed = db.RiceSeeds.Find(id);
            if (riceSeed == null)
            {
                return HttpNotFound();
            }
            return View(riceSeed);
        }

        // POST: RiceSeeds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var obj = db.RiceSeeds.Find(id);
            try
            {
                db.RiceSeeds.Remove(obj);
                db.SaveChanges();
                TempData["SuccessMessage"] = " deleted successfully";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                if (obj == null)
                {
                    return HttpNotFound();
                }
                TempData["ErrorMessage"] = "You can't delete this.";
                return View(obj);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
