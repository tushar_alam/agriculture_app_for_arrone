﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AgricultureManagementSystem.Helper;
using AgricultureManagementSystem.Models;
using AgricultureManagementSystem.Models.ViewModels;

namespace AgricultureManagementSystem.Controllers
{
    [SessionExpire]
    public class DistributionOfSeedsController : Controller
    {
        private AMSEntities db = new AMSEntities();

        // GET: DistributionOfSeeds
        public ActionResult Index()
        {
            var distributionOfSeeds = db.DistributionOfSeeds.Include(d => d.Farmer).Include(d => d.SeedType);
            return View(distributionOfSeeds.ToList());
        }

        // GET: DistributionOfSeeds/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DistributionOfSeed distributionOfSeed = db.DistributionOfSeeds.Find(id);
            if (distributionOfSeed == null)
            {
                return HttpNotFound();
            }
            return View(distributionOfSeed);
        }

        // GET: DistributionOfSeeds/Create
        public ActionResult Create()
        {
            ViewBag.FarmerId = new SelectList(db.Farmers, "Id", "FirstName");
            ViewBag.TypeId = new SelectList(db.SeedTypes, "Id", "Name");
            return View();
        }

        // POST: DistributionOfSeeds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DistributionVm distributionVm)
        {
            if (distributionVm != null)
            {
                var distribution = new DistributionOfSeed
                {
                    Id = distributionVm.Id,
                    Date = distributionVm.Date,
                    FarmerId = distributionVm.FarmerId,
                    Address = distributionVm.Address,
                    Area = distributionVm.Area,
                    SeedTypeId = distributionVm.TypeId,
                    Quantity = distributionVm.Quantity,
                };

                db.DistributionOfSeeds.Add(distribution);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FarmerId = new SelectList(db.Farmers, "Id", "FirstName", distributionVm.FarmerId);
            ViewBag.TypeId = new SelectList(db.SeedTypes, "Id", "Name", distributionVm.TypeId);
            return View(distributionVm);
        }

        // GET: DistributionOfSeeds/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DistributionOfSeed distributionVm = db.DistributionOfSeeds.Find(id);
            if (distributionVm == null)
            {
                return HttpNotFound();
            }
            var distribution = new DistributionVm
            {
                Id = distributionVm.Id,
                Date = distributionVm.Date,
                FarmerId = distributionVm.FarmerId,
                Address = distributionVm.Address,
                Area = distributionVm.Area,
                TypeId = distributionVm.SeedTypeId,
                Quantity = distributionVm.Quantity,
            };
            ViewBag.FarmerId = new SelectList(db.Farmers, "Id", "FirstName", distribution.FarmerId);
            ViewBag.TypeId = new SelectList(db.SeedTypes, "Id", "Name", distribution.TypeId);
            return View(distribution);
        }

        // POST: DistributionOfSeeds/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( DistributionVm distributionVm)
        {
            if (distributionVm != null)
            {
                var distribution = new DistributionOfSeed
                {
                    Id = distributionVm.Id,
                    Date = distributionVm.Date,
                    FarmerId = distributionVm.FarmerId,
                    Address = distributionVm.Address,
                    Area = distributionVm.Area,
                    SeedTypeId = distributionVm.TypeId,
                    Quantity = distributionVm.Quantity,
                };
                db.Entry(distribution).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FarmerId = new SelectList(db.Farmers, "Id", "FirstName", distributionVm.FarmerId);
            ViewBag.TypeId = new SelectList(db.SeedTypes, "Id", "Name", distributionVm.TypeId);
            return View(distributionVm);
        }

        // GET: DistributionOfSeeds/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DistributionOfSeed distributionOfSeed = db.DistributionOfSeeds.Find(id);
            if (distributionOfSeed == null)
            {
                return HttpNotFound();
            }
            return View(distributionOfSeed);
        }

        // POST: DistributionOfSeeds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DistributionOfSeed distributionOfSeed = db.DistributionOfSeeds.Find(id);
            db.DistributionOfSeeds.Remove(distributionOfSeed);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
