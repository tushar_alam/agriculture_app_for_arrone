﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AgricultureManagementSystem.Helper;
using AgricultureManagementSystem.Models;
using AgricultureManagementSystem.Models.ViewModels;

namespace AgricultureManagementSystem.Controllers
{
    [SessionExpire]
    public class FarmersController : Controller
    {
        private AMSEntities db = new AMSEntities();

        // GET: Farmers
        public ActionResult Index()
        {
            return View(db.Farmers.ToList());
        }

        // GET: Farmers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Farmer farmer = db.Farmers.Find(id);
            if (farmer == null)
            {
                return HttpNotFound();
            }
            return View(farmer);
        }

        // GET: Farmers/Create
        public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FarmerVm farmerVm)
        {
            var farmer = new Farmer
            {
                FirstName = farmerVm.FirstName,
                MiddleName = farmerVm.MiddleName,
                LastName = farmerVm.LastName,
                Province = farmerVm.Province,
                Municipality = farmerVm.Municipality,
                SmallTown = farmerVm.SmallTown,
                BirthDate = farmerVm.BirthDate,
                Gender = farmerVm.Gender,
                IDNo = farmerVm.IDNo,
                Email = farmerVm.Email,
                ContactNo = farmerVm.ContactNo,
                Status = 1
            };

            if (farmer != null)
            {
                db.Farmers.Add(farmer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(farmer);
        }

        // GET: Farmers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var farmerVm = db.Farmers.Find(id);

            var farmer = new FarmerVm
            {
                FirstName = farmerVm.FirstName,
                MiddleName = farmerVm.MiddleName,
                LastName = farmerVm.LastName,
                Province = farmerVm.Province,
                Municipality = farmerVm.Municipality,
                SmallTown = farmerVm.SmallTown,
                BirthDate = farmerVm.BirthDate,
                Gender = farmerVm.Gender,
                IDNo = farmerVm.IDNo,
                Email = farmerVm.Email,
                ContactNo = farmerVm.ContactNo,
                Status = 1
            };
            if (farmer == null)
            {
                return HttpNotFound();
            }
            return View(farmer);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FarmerVm farmerVm)
        {
            var farmer = new Farmer
            {
                Id=farmerVm.Id,
                FirstName = farmerVm.FirstName,
                MiddleName = farmerVm.MiddleName,
                LastName = farmerVm.LastName,
                Province = farmerVm.Province,
                Municipality = farmerVm.Municipality,
                SmallTown = farmerVm.SmallTown,
                BirthDate = farmerVm.BirthDate,
                Gender = farmerVm.Gender,
                IDNo = farmerVm.IDNo,
                Email = farmerVm.Email,
                ContactNo = farmerVm.ContactNo,
                Status = 1
            };

            if (farmer != null)
            {
                db.Entry(farmer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(farmer);
        }

        // GET: Farmers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Farmer farmer = db.Farmers.Find(id);
            if (farmer == null)
            {
                return HttpNotFound();
            }
            return View(farmer);
        }

        // POST: Farmers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Farmer farmer = db.Farmers.Find(id);
            db.Farmers.Remove(farmer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
