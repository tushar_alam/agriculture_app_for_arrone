﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AgricultureManagementSystem.Helper;
using AgricultureManagementSystem.Models;
using AgricultureManagementSystem.Models.ViewModels;

namespace AgricultureManagementSystem.Controllers
{
    [SessionExpire]
    public class UserAccountsController : Controller
    {
        private AMSEntities db = new AMSEntities();

        // GET: UserAccounts
        public ActionResult Index()
        {
            var userAccounts = db.UserAccounts.Include(u => u.Farmer);
            return View(userAccounts.ToList());
        }

        // GET: UserAccounts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserAccount userAccount = db.UserAccounts.Find(id);
            if (userAccount == null)
            {
                return HttpNotFound();
            }
            return View(userAccount);
        }

        // GET: UserAccounts/Create
        public ActionResult Create()
        {
            ViewBag.FarmerId = new SelectList(db.Farmers, "Id", "FirstName");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserAccountVm userAccountVm)
        {
            var userAccount = new UserAccount
            {
                Id = userAccountVm.Id,
                FarmerId = userAccountVm.FarmerId,
                UserName = userAccountVm.UserName,
                Password = userAccountVm.Password,
                Status = 1,
            };

            if (ModelState.IsValid)
            {
                db.UserAccounts.Add(userAccount);
                db.SaveChanges();
                TempData["SuccessMessage"] = "Created successfully";
                return RedirectToAction("Index");
            }
            TempData["ErrorMessage"] = "Create faild";
            ViewBag.FarmerId = new SelectList(db.Farmers, "Id", "FirstName", userAccount.FarmerId);
            return View(userAccount);
        }

        // GET: UserAccounts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserAccount userAccountVm = db.UserAccounts.Find(id);
            var userAccount = new UserAccountVm
            {
                Id = userAccountVm.Id,
                FarmerId = userAccountVm.FarmerId,
                UserName = userAccountVm.UserName,
                Password = userAccountVm.Password,
                Status = userAccountVm.Status,
            };
            if (userAccount == null)
            {
                return HttpNotFound();
            }
            ViewBag.FarmerId = new SelectList(db.Farmers, "Id", "FirstName", userAccount.FarmerId);
            return View(userAccount);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserAccountVm userAccountVm)
        {
            var userAccount = new UserAccount
            {
                Id = userAccountVm.Id,
                FarmerId = userAccountVm.FarmerId,
                UserName = userAccountVm.UserName,
                Password = userAccountVm.Password,
                Status = userAccountVm.Status,
            };
            if (ModelState.IsValid)
            {
                db.Entry(userAccount).State = EntityState.Modified;
                db.SaveChanges();
                TempData["SuccessMessage"] = "update successfully";
                return RedirectToAction("Index");
            }
            TempData["ErrorMessage"] = "update faild";
            ViewBag.FarmerId = new SelectList(db.Farmers, "Id", "FirstName", userAccount.FarmerId);
            return View(userAccount);
        }

        // GET: UserAccounts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserAccount userAccount = db.UserAccounts.Find(id);
            if (userAccount == null)
            {
                return HttpNotFound();
            }
            return View(userAccount);
        }

        // POST: UserAccounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var obj = db.UserAccounts.Find(id);
            try
            {
                db.UserAccounts.Remove(obj);
                db.SaveChanges();
                TempData["SuccessMessage"] = " deleted successfully";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                if (obj == null)
                {
                    return HttpNotFound();
                }
                TempData["ErrorMessage"] = "You can't delete this.";
                return View(obj);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
