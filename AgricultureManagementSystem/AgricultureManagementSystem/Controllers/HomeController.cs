﻿using AgricultureManagementSystem.Models;
using AgricultureManagementSystem.Models.ViewModels;
using AgricultureManagementSystem.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AgricultureManagementSystem.Controllers
{
    public class HomeController : Controller
    {
        private readonly AMSEntities db = new AMSEntities();

        [SessionExpire]
        public ActionResult Dashboard()
        {
            return View();
        }

        public ActionResult Index()
        {
            if (Session["username"]!=null)
            {
                Session["username"] = null;
            }
            if (Session["farmerId"] != null)
            {
                Session["farmerId"] = null;
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(UserVm user)
        {
            var account = db.UserAccounts.FirstOrDefault(x => x.UserName == user.Username && x.Password == user.Password);
            if (account != null)
            {
                Session["username"] = account.UserName;
                Session["farmerId"] = account.FarmerId;
                return RedirectToAction("Dashboard", "Home");
            }
            TempData["ErrorMessage"] = "Incorrect username or password";
            return View();
        }


        [HttpPost]
        public ActionResult Registration(UserVm user)
        {
            var farmer = new Farmer
            {
                FirstName = user.Username,
                MiddleName = "",
                LastName = "",
                Province = "",
                Municipality = "",
                SmallTown="",
                BirthDate = DateTime.Now,
                Gender = "",
                IDNo = "",
                Email = "",
                ContactNo="",
                Status=1
            };
            db.Farmers.Add(farmer);
            int affectedRow = db.SaveChanges();
            if (affectedRow > 0)
            {
                var account = new UserAccount
                {
                    FarmerId = farmer.Id,
                    UserName = user.Username,
                    Password = user.Password,
                    Status = 1,
                };

                db.UserAccounts.Add(account);
                int affectedRow2 = db.SaveChanges();
                if (affectedRow2 > 0)
                {
                    Session["username"] = account.UserName;
                    Session["farmerId"] = account.FarmerId;
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            TempData["ErrorMessage"] = "Registration Faild";
            return View();
        }
    }
}