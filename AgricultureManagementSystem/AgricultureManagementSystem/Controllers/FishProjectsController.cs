﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AgricultureManagementSystem.Helper;
using AgricultureManagementSystem.Models;
using AgricultureManagementSystem.Models.ViewModels;

namespace AgricultureManagementSystem.Controllers
{
    [SessionExpire]
    public class FishProjectsController : Controller
    {
        private AMSEntities db = new AMSEntities();

        // GET: FishProjects
        public ActionResult Index()
        {
            return View(db.FishProjects.ToList());
        }

        // GET: FishProjects/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FishProject fishProject = db.FishProjects.Find(id);
            if (fishProject == null)
            {
                return HttpNotFound();
            }
            return View(fishProject);
        }

        // GET: FishProjects/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FishPeojectVm fishProjectVm)
        {
            var fishProject = new FishProject
            {
                Id = fishProjectVm.Id,
                Name = fishProjectVm.Name,
                Description = fishProjectVm.Description,
                StartDate = fishProjectVm.StartDate,
                EndDate = fishProjectVm.EndDate,
            };
            if (ModelState.IsValid)
            {
                db.FishProjects.Add(fishProject);
                db.SaveChanges();
                TempData["SuccessMessage"] = "Created successfully";
                return RedirectToAction("Index");
            }
            TempData["ErrorMessage"] = "Create faild";
            return View(fishProject);
        }

        // GET: FishProjects/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var fishProjectVm = db.FishProjects.Find(id);

            var fishProject = new FishPeojectVm
            {
                Id = fishProjectVm.Id,
                Name = fishProjectVm.Name,
                Description = fishProjectVm.Description,
                StartDate = fishProjectVm.StartDate,
                EndDate = fishProjectVm.EndDate,
            };
            if (fishProject == null)
            {
                return HttpNotFound();
            }
            return View(fishProject);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FishPeojectVm fishProjectVm)
        {
            var fishProject = new FishProject
            {
                Id = fishProjectVm.Id,
                Name = fishProjectVm.Name,
                Description = fishProjectVm.Description,
                StartDate = fishProjectVm.StartDate,
                EndDate = fishProjectVm.EndDate,
            };
            if (ModelState.IsValid)
            {
                db.Entry(fishProject).State = EntityState.Modified;
                db.SaveChanges();
                TempData["SuccessMessage"] = "update successfully";
                return RedirectToAction("Index");
            }
            TempData["ErrorMessage"] = "update faild";
            return View(fishProject);
        }

        // GET: FishProjects/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FishProject fishProject = db.FishProjects.Find(id);
            if (fishProject == null)
            {
                return HttpNotFound();
            }
            return View(fishProject);
        }

        // POST: FishProjects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var obj = db.FishProjects.Find(id);
            try
            {
                db.FishProjects.Remove(obj);
                db.SaveChanges();
                TempData["SuccessMessage"] = " deleted successfully";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                if (obj == null)
                {
                    return HttpNotFound();
                }
                TempData["ErrorMessage"] = "You can't delete this.";
                return View(obj);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
