﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AgricultureManagementSystem.Helper;
using AgricultureManagementSystem.Models;
using AgricultureManagementSystem.Models.ViewModels;

namespace AgricultureManagementSystem.Controllers
{
    [SessionExpire]
    public class ReceiveOfProductsController : Controller
    {
        private AMSEntities db = new AMSEntities();

        // GET: ReceiveOfProducts
        public ActionResult Index()
        {
            return View(db.ReceiveOfProducts.ToList());
        }

        // GET: ReceiveOfProducts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReceiveOfProduct receiveOfProduct = db.ReceiveOfProducts.Find(id);
            if (receiveOfProduct == null)
            {
                return HttpNotFound();
            }
            return View(receiveOfProduct);
        }

        // GET: ReceiveOfProducts/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ReceiveOfProducts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ArriveDate,ProductName")] ReceiveOfProductVm receiveOfProductVm)
        {
            if (receiveOfProductVm != null)
            {
                var receiveOfProduct = new ReceiveOfProduct
                {
                    Id = receiveOfProductVm.Id,
                    ArriveDate = receiveOfProductVm.ArriveDate,
                    ProductName = receiveOfProductVm.ProductName
                };

                db.ReceiveOfProducts.Add(receiveOfProduct);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(receiveOfProductVm);
        }

        // GET: ReceiveOfProducts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReceiveOfProduct receiveOfProductVm = db.ReceiveOfProducts.Find(id);
            if (receiveOfProductVm == null)
            {
                return HttpNotFound();
            }

            var receiveOfProduct = new ReceiveOfProductVm
            {
                Id = receiveOfProductVm.Id,
                ArriveDate = receiveOfProductVm.ArriveDate,
                ProductName = receiveOfProductVm.ProductName
            };
            return View(receiveOfProduct);
        }

        // POST: ReceiveOfProducts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ArriveDate,ProductName")] ReceiveOfProductVm receiveOfProductVm)
        {
            if (receiveOfProductVm != null)
            {
                var receiveOfProduct = new ReceiveOfProduct
                {
                    Id = receiveOfProductVm.Id,
                    ArriveDate = receiveOfProductVm.ArriveDate,
                    ProductName = receiveOfProductVm.ProductName
                };

                db.Entry(receiveOfProduct).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(receiveOfProductVm);
        }

        // GET: ReceiveOfProducts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReceiveOfProduct receiveOfProduct = db.ReceiveOfProducts.Find(id);
            if (receiveOfProduct == null)
            {
                return HttpNotFound();
            }
            return View(receiveOfProduct);
        }

        // POST: ReceiveOfProducts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ReceiveOfProduct receiveOfProduct = db.ReceiveOfProducts.Find(id);
            db.ReceiveOfProducts.Remove(receiveOfProduct);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
