﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AgricultureManagementSystem.Models;
using AgricultureManagementSystem.Models.ViewModels;

namespace AgricultureManagementSystem.Controllers
{
    public class SeedTypesController : Controller
    {
        private AMSEntities db = new AMSEntities();

        // GET: SeedTypes
        public ActionResult Index()
        {
            return View(db.SeedTypes.ToList());
        }

        // GET: SeedTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SeedType seedType = db.SeedTypes.Find(id);
            if (seedType == null)
            {
                return HttpNotFound();
            }
            return View(seedType);
        }

        // GET: SeedTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SeedTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name")] SeedTypeVm seedTypeVm)
        {
            if (seedTypeVm != null)
            {
                var seedType = new SeedType
                {
                    Id = seedTypeVm.Id,
                    Name = seedTypeVm.Name
                };

                db.SeedTypes.Add(seedType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(seedTypeVm);
        }

        // GET: SeedTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SeedType seedTypeVm = db.SeedTypes.Find(id);
            if (seedTypeVm == null)
            {
                return HttpNotFound();
            }

            var seedType = new SeedTypeVm
            {
                Id = seedTypeVm.Id,
                Name = seedTypeVm.Name
            };
            return View(seedType);
        }

        // POST: SeedTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] SeedTypeVm seedTypeVm)
        {
            if (seedTypeVm != null)
            {
                var seedType = new SeedType
                {
                    Id = seedTypeVm.Id,
                    Name = seedTypeVm.Name
                };
                db.Entry(seedType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(seedTypeVm);
        }

        // GET: SeedTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SeedType seedType = db.SeedTypes.Find(id);
            if (seedType == null)
            {
                return HttpNotFound();
            }
            return View(seedType);
        }

        // POST: SeedTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SeedType seedType = db.SeedTypes.Find(id);
            db.SeedTypes.Remove(seedType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
