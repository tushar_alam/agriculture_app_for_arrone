﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AgricultureManagementSystem.Helper;
using AgricultureManagementSystem.Models;

namespace AgricultureManagementSystem.Controllers
{
    [SessionExpire]
    public class SaplingSeedsController : Controller
    {
        private AMSEntities db = new AMSEntities();

        // GET: SaplingSeeds
        public ActionResult Index()
        {
            var saplingSeeds = db.SaplingSeeds.Include(s => s.SeedType);
            return View(saplingSeeds.ToList());
        }

        // GET: SaplingSeeds/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SaplingSeed saplingSeed = db.SaplingSeeds.Find(id);
            if (saplingSeed == null)
            {
                return HttpNotFound();
            }
            return View(saplingSeed);
        }

        // GET: SaplingSeeds/Create
        public ActionResult Create()
        {
            ViewBag.SeedTypeId = new SelectList(db.SeedTypes, "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Date,SeedTypeId,Quantity")] SaplingSeed saplingSeed)
        {
            if (ModelState.IsValid)
            {
                db.SaplingSeeds.Add(saplingSeed);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SeedTypeId = new SelectList(db.SeedTypes, "Id", "Name", saplingSeed.SeedTypeId);
            return View(saplingSeed);
        }

        // GET: SaplingSeeds/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SaplingSeed saplingSeed = db.SaplingSeeds.Find(id);
            if (saplingSeed == null)
            {
                return HttpNotFound();
            }
            ViewBag.SeedTypeId = new SelectList(db.SeedTypes, "Id", "Name", saplingSeed.SeedTypeId);
            return View(saplingSeed);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Date,SeedTypeId,Quantity")] SaplingSeed saplingSeed)
        {
            if (ModelState.IsValid)
            {
                db.Entry(saplingSeed).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SeedTypeId = new SelectList(db.SeedTypes, "Id", "Name", saplingSeed.SeedTypeId);
            return View(saplingSeed);
        }

        // GET: SaplingSeeds/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SaplingSeed saplingSeed = db.SaplingSeeds.Find(id);
            if (saplingSeed == null)
            {
                return HttpNotFound();
            }
            return View(saplingSeed);
        }

        // POST: SaplingSeeds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SaplingSeed saplingSeed = db.SaplingSeeds.Find(id);
            db.SaplingSeeds.Remove(saplingSeed);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
